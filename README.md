# Object Detection

This is the object detection exercise for FL8 week 6 by Gilad Rosenblatt.

## Architecture

![approch2](/assets/design.png?raw=true)

## License

[WTFPL](http://www.wtfpl.net/)
